const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    create(data) {
        return FighterRepository.create(data);
    }

    update(id, data) {
        return FighterRepository.update(id, data);
    }

    delete(id) {
        return FighterRepository.delete(id);
    }

    getAll() {
        return FighterRepository.getAll();
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();