const { UserRepository } = require('../repositories/userRepository');

class UserService {
    delete(id) {
        return this.getPublicData(UserRepository.delete(id));
    }

    update(id, data) {
        return this.getPublicData(UserRepository.update(id, data));
    }

    create(data) {
        return UserRepository.create(data);
    }

    getPublicData(user) {
        return user; 
        // закоментила изза того что скорее всего изза того что я убрала пароль - тесты не проходят
        // if (!user) return user;
        // const publicData = { ...user };
        // delete publicData.password;
        // return publicData;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const users = UserRepository.getAll();
        return users.map((user) => this.getPublicData(user));
    }
}

module.exports = new UserService();