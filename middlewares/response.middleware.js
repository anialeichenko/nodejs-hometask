const responseMiddleware = (req, res, next) => {
    if (res.err) {
        if (res.statusCode === 200) {
            res.status(400);
        }
        res.send(JSON.stringify({
            error: true, 
            message: res.err.message,
        }));
    } else {
        res.send(JSON.stringify(res.data));
    }
}

exports.responseMiddleware = responseMiddleware;