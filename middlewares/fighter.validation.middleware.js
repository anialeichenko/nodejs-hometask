const FighterService = require('../services/fighterService');
const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    try {
        const data = req.body;

        const requiredFields = ['name', 'power', 'defense'];
        requiredFields.forEach((field) => {
            if(!data[field]) {
                throw new Error(`${field} is required`);
            }  
        });
        
        const allowedFields = [...requiredFields, 'health']
        const dennyFields = Object.keys(data).filter((key) => !allowedFields.includes(key));
        if (dennyFields.length) {
            throw new Error(`Denny fields: ${dennyFields}`);
        }

        if(Number.isNaN(+data.power) || data.power <= 1 || data.power >= 100) {
            throw new Error('Power number have to be more than 1 and less than 100');
        }

        if(Number.isNaN(+data.defense) || data.defense <= 1 || data.defense >= 10) {
            throw new Error('Defense number have to be more than 1 and less than 10');
        }

        data.health = data.health || 100;
        if(Number.isNaN(+data.health) || data.health <= 80 || data.health >= 120) {
            throw new Error('Health number have to be more than 80 and less than 100');
        }

        const fighterWithSameName = FighterService.search({
            name: data.name,
        });
        if (fighterWithSameName) {
            throw new Error("Fighter with this name already exist");
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    try {
        const data = req.body;

        const alloweddFields = Object.keys(fighter).filter(name => name !== 'id');
        
        const receivedFields = Object.keys(data);
        const dennyFields = receivedFields.filter((key) => !alloweddFields.includes(key));
        if (dennyFields.length) {
            throw new Error(`Denny fields: ${dennyFields}`);
        }
        
        if(receivedFields.length - dennyFields.length === 0) {
            throw new Error('At least one field required to update');
        }
        
        if(data.power && (Number.isNaN(+data.power) || data.power <= 1 || data.power >= 100)) {
            throw new Error('Power number have to be more than 1 and less than 100');
        }

        if(data.defense && (Number.isNaN(+data.defense) || data.defense <= 1 || data.defense >= 10)) {
            throw new Error('Defense number have to be more than 1 and less than 10');
        }

        if (data.health) {
            const health = data.health || 100;
            if(Number.isNaN(+health) || health <= 80 || health >= 120) {
                throw new Error('Health number have to be more than 80 and less than 100');
            }
        }
        
        if (data.name) {
            const fighterWithSameName = FighterService.search({
                name: data.name,
            });
            if (fighterWithSameName) {
                throw new Error("Fighter with this name already exist");
            }
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;