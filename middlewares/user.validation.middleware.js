const UserService = require('../services/userService');
const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    try {
        const data = req.body;

        const requiredFields = Object.keys(user).filter(name => name !== 'id');
        requiredFields.forEach((field) => {
            if(!data[field]) {
                throw new Error(`${field} is required`);
            }  
        });

        const dennyFields = Object.keys(data).filter((key) => !requiredFields.includes(key));
        if (dennyFields.length) {
            throw new Error(`Denny fields: ${dennyFields}`);
        }

        if(!data.email.endsWith('@gmail.com')) {
            throw new Error('Email should use @gmail.com domain');
        }

        const phoneNumberRegExp = new RegExp(/^\+380\d{9,9}$/);
        if(!phoneNumberRegExp.test(data.phoneNumber)) {
            throw new Error('Phone number have to have correct fromat +380xxxxxxxxx');
        }

        if(data.password.length < 3) {
            throw new Error('Password length should at least 3 symbols');
        }
        
        const userWithSameEmail = UserService.search({
            email: data.email,
        });
        if (userWithSameEmail) {
            throw new Error("User with this email already exist");
        }

        const userWithSamePhoneNumber = UserService.search({
            phoneNumber: data.phoneNumber,
        });
        if (userWithSamePhoneNumber) {
            throw new Error("User with this phone number already exist");
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    try {
        const data = req.body;

        const alloweddFields = Object.keys(user).filter(name => name !== 'id');
        
        const receivedFields = Object.keys(data);
        const dennyFields = receivedFields.filter((key) => !alloweddFields.includes(key));
        if (dennyFields.length) {
            throw new Error(`Denny fields: ${dennyFields}`);
        }

        if(receivedFields.length - dennyFields.length === 0) {
            throw new Error('At least one field required to update');
        }

        if(data.email) {
            if(!data.email.endsWith('@gmail.com')) {
                throw new Error('Email should use @gmail.com domain');
            }
            const userWithSameEmail = UserService.search({
                email: data.email,
            });
            if (userWithSameEmail) {
                throw new Error("User with this email already exist");
            }
        }

        if(data.phoneNumber) {
            const phoneNumberRegExp = new RegExp(/^\+380\d{9,9}$/);
            if(!phoneNumberRegExp.test(data.phoneNumber)) {
                throw new Error('Phone number have to have correct fromat +380xxxxxxxxx');
            }
            const userWithSamePhoneNumber = UserService.search({
                phoneNumber: data.phoneNumber,
            });
            if (userWithSamePhoneNumber) {
                throw new Error("User with this phone number already exist");
            }
        }

        if(data.password && data.password.length < 3) {
            throw new Error('Password length should at least 3 symbols');
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;