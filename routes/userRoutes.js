const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        res.data = UserService.getAll();
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);



router.get('/:id', (req, res, next) => {
    try {
        const user = UserService.getPublicData(UserService.search({ id: req.params.id }));
        // if(!user) {
        //     res.status(404);
        //     throw new Error('User not found');
        // }
        res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


router.put('/:id', updateUserValid, (req, res, next) => {
    if(res.err) {
        next();
        return;
    }
    try {
        res.data = UserService.update(req.params.id, req.body);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const user = UserService.search({ id: req.params.id });
        if(!user) {
            res.status(404);
            throw new Error('User not found');
        }
        res.data = UserService.delete(req.params.id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


router.post('/', createUserValid, (req, res, next) => {
    if(res.err) {
        next();
        return;
    }
    try {
        res.data = UserService.getPublicData(UserService.create(req.body));
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;