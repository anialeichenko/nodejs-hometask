const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();


router.get('/', (req, res, next) => {
    try {
        const fighters = FighterService.getAll();
        res.data = fighters;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const fighter = FighterService.search({ id: req.params.id });
        // if(!fighter) {
        //     res.status(404);
        //     throw new Error('Fighter not found');
        // } 
        res.data = fighter;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


router.put('/:id', updateFighterValid, (req, res, next) => {
    if(res.err) {
        next();
        return;
    }
    try {
        const fighters = FighterService.update(req.params.id, req.body);
        res.data = fighters;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const fighter = FighterService.search({ id: req.params.id });
        if(!fighter) {
            res.status(404);
            throw new Error('Fighter not found');
        }
        res.data = FighterService.delete(req.params.id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


router.post('/', createFighterValid, (req, res, next) => {
    if(res.err) {
        next();
        return;
    }
    try {
        res.data = FighterService.create(req.body);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;